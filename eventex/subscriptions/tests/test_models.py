# coding: utf-8
from django.test import TestCase
from django.db import IntegrityError
from datetime import datetime
from eventex.subscriptions.models import Subscription


class SubscriptionTest(TestCase):
    def setUp(self):
        self.obj = Subscription(
            name='Eric Onuki',
            cpf='12345678901',
            email='ericonuki@gmail.com',
            phone='41-9631-9620'
        )

    def test_create(self):
        'Subscription must have name, cpf, email, phone'
        self.obj.save()
        self.assertEqual(1, self.obj.id)

    def test_has_created_at(self):
        'Subscription must have automatic created_at'
        self.obj.save()
        self.assertIsInstance(self.obj.created_at,datetime)

    def test_unicode(self):
        self.assertEqual(u'Eric Onuki',unicode(self.obj))

    def test_paid_default_value_is_False(self):
        'By default paid must be False.'
        self.assertEqual(False, self.obj.paid)


class SubscriptionUniqueTest(TestCase):
    def setUp(self):
        # Create a first entry to force the colision
        Subscription.objects.create(name='Eric Onuki', cpf='12345678901',
                                    email='ericonuki@gmail.com', phone='41-96319620')

    def test_cpf_unique(self):
        'CPF must be unique'
        s = Subscription(name='Eric Onuki', cpf='12345678901',
                         email='outro@email.com', phone='41-96319620')
        self.assertRaises(IntegrityError, s.save)

##    def test_email_unique(self):
##        'Email must be unique'
##        s = Subscription(name='Eric Onuki', cpf='00000000011',
##                         email='ericonuki@gmail.com',)
##        self.assertRaises(IntegrityError, s.save)

    def test_email_can_repeat(self):
        'Email is not unique anymore'
        s = Subscription.objects.create(name='Eric Onuki', cpf='19876543210',email='ericonuki@gmail.com',phone='41-96319620')
        self.assertEqual(2, s.pk)